import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';// es necesatio para usar el ngModule
//npm install @angular/forms para actualizar
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';

// angular material
/* import { AngularMaterialModule } from './angular-material/angular-material.module';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import { ErrorStateMatcher, ShowOnDirtyErrorStateMatcher } from '@angular/material/core';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar'; */

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { HomeComponent } from './pages/home/home.component';
import { FooterComponent } from './components/footer/footer.component';
import { CarrucelComponent } from './components/carrucel/carrucel.component';
import { MenuLeftComponent } from './components/menu-left/menu-left.component';
import { OfertasComponent } from './components/ofertas/ofertas.component';
import { PrendasComponent } from './components/ofertas/prendas/prendas.component';
import { ModelosComponent } from './components/modelos/modelos.component';
import { PrendaComponent } from './components/prenda/prenda.component';
import { TopRegisterComponent } from './components/menu/top-register/top-register.component';
import { MedidasComponent } from './components/ofertas/prendas/medidas/medidas.component';
import { OfertacarrucelComponent } from './components/ofertas/carrucel/ofertacarrucel.component';
import { SliderComponent } from './components/ofertas/prendas/slider/slider.component';
import { HeadComponent } from './components/menu/head/head.component';
import { SobreNosotrosComponent } from './pages/sobre-nosotros/sobre-nosotros.component';
import { ContactoComponent } from './pages/contacto/contacto.component';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HomeComponent,
    FooterComponent,
    CarrucelComponent,
    MenuLeftComponent,
    OfertasComponent,
    PrendasComponent,
    ModelosComponent,
    PrendaComponent,
    TopRegisterComponent,
    MedidasComponent,
    OfertacarrucelComponent,
    SliderComponent,
    HeadComponent,
    SobreNosotrosComponent,
    ContactoComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
     /* ReactiveFormsModule, */
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,

    /* AngularMaterialModule,
    MatToolbarModule,
    MatIconModule, */
  ],
  providers: [
   /*  {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher},
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2500}} */
  ],
  //entryComponents:[ModalsComponent,MsnSnackBarComponent],
  bootstrap: [AppComponent]
  //schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
