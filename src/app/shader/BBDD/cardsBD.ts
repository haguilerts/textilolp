import { Cards } from "../interfaces/cards";

/* import{cards} from'../interfaces/cards'; */
export const Producto:Cards[]=[
  {
    id:1,
    tittle:'Sombrero',
    discount:10,
    data:'Sombrero de Verano con tapa nuca',
    price:1000,
    img:'./../../../../assets/Prendas/sombreros/sombrero1.jpeg',
    caracteristicas:{
      /* marca:"olp",
      modelo:"sombreso",
      Talles:["1", "2"],
      colores:["azul marino", "negro"],
      genero:"masculino",
      tela:"poliester",
      medida:"40cm Diamtro", */
      detalles:[

        {name:'Marca', value:'Textil'},
        {name:'Temporada', value:'Verano'},
        {name:'Tipo de Tela/material', value:' tela xxxx'},
        {name:'Genero', value:'Masculino/Femenino'},
        {name:'Talles', value:['s',' m',' l',' xl',' xxl']},
        {name:'Color/es', value:['Azul Marino', 'Negro', 'Multicolor']},
        {name:'medida', value:'60cm Diametro'},
      ],
      img:[
        "./.././../../../../assets/Prendas/sombreros/sombrero1.jpeg",
        "./.././../../../../assets/Prendas/sombreros/sombrero2.jpeg",
        "./.././../../../../assets/Prendas/sombreros/sombrero3.jpeg",
      ]
    },
    Descripcion:[
      {name:'SOMBRERO DE VERANO 2021',
        value:'Sombrero en lana confort (suave) con la capacidad de recuperar su forma después de doblarse. Toquilla en cuero recuperado con aplicación de cristales y herrajes plata. Cinta acolchonada en el interior. Diametro: 35.5cm // Altura: 11.5cm'},
      {name:'RECOMENDACION',
        value:'Le recomendamos no guardar su sombrero sobre el ala para evitar que se deforme. Mantener alejado de la humedad. Para limpiarlo utilice un paño limpio y ligeramente húmedo, evite a toda costa contacto con alcohol, grasa y abrasivos.'}
    ]
  },
  {
    id:2,
    tittle:'Pechera',
    discount:5,
    data:'pechera simples personalizado',
    price:500,
    img:'../../../assets/Prendas/pecheras/pechera_1.jpg',
    caracteristicas:{
      detalles:[
        {name:'Marca', value:'Olp'},
        {name:'Temporada', value:'Verano'},
        {name:'Tipo de Tela/material', value:'Acetato'},
        {name:'Genero', value:'Masculino/Femenino'},
        {name:'Talles', value:['s',' m',' l',' xl',' xxl']},
        {name:'Color/es', value:['Azul Marino']},
        {name:'medida', value:'60x35cm'},
        
      ],
      img:[
        "./.././../../../../assets/Prendas/pecheras/pechera_1.jpg",
        "./.././../../../../assets/Prendas/pecheras/pechera_2.jpg",
      ]
    },
    Descripcion:[
      {name:'PECHERA DE GRAN USO',
        value:`
        Esta prenda fue diseñada exclusivamente para acompañarte en El Cruce
        y en todos tu uso.  
        `},
      {name:'RECOMENDACION',
        value:'Es indispensable usar correctamente...'},
      {name:'',
        value:'En caso de precisar colores y talles surtidos es necesario elegir exactamente el color y talle que vas a llevar con la opción'}
    ]
  },
  {
    id:3,
    tittle:'Bolsa de Compra',
    discount:1,
    data:'Bolsa ligera Ecologica',
    price:200,
    img:'../../../assets/Prendas/bolsas/modelo1/bolsa4.jpeg',
    caracteristicas:{
      detalles:[
        {name:'Marca', value:'OLP Textil'},
        {name:'Temporada', value:'Verano/Invierno'},
        {name:'Tipo de Tela/material', value:' Ecologicas'},
        {name:'Genero', value:'Masculino/Femenino'},
        {name:'Talles', value:['4,5']},
        {name:'Color/es', value:['Blanco, Azul, Multicolor']},
        {name:'medida', value:'50x50 cm'},
      ],
      img:[
        "./.././../../../../assets/Prendas/bolsas/modelo1/bolsa_1.jpg",
        "./.././../../../../assets/Prendas/bolsas/modelo1/bolsa_2.jpg",
        "./.././../../../../assets/Prendas/bolsas/modelo1/bolsa1.jpeg",
        "./.././../../../../assets/Prendas/bolsas/modelo1/bolsa2.jpeg",
        "./.././../../../../assets/Prendas/bolsas/modelo1/bolsa3.jpeg",
        "./.././../../../../assets/Prendas/bolsas/modelo1/bolsa4.jpeg",
      ]
    },
    Descripcion:[
      {name:'BOLSA SUPERMERCADO',
        value:'Es bueno elegir bolsas reutilizables para llevar en el coche, en el bolso o en la mochila. Para hacer la compra, es fundamental tener un carro y acordarse de lo que queremos comprar, llevando una lista.'},
      {name:'',
        value:`
        Si se quiere una bolsa práctica que no ocupa nada y que se pueda guardar en un bolsillo , en el bolso o dejar en el lateral del coche para cuando  haga falta, esta bolsa tipo crochet es perfecta. Por menos tienes una bolsa con gran capacidad y no ocupa prácticamente nada.
        `}
    ]
  },
  {
    id:4,
    tittle:'Bolsa Aqualane',
    discount:1,
    data:'Bolsa personalizada',
    price:200,
    img:'../../../assets/Prendas/bolsas/modelo2/bolsa1.jpeg',
    caracteristicas:{
      detalles:[
        {name:'Marca', value:'Textil OLP'},
        {name:'Temporada', value:'Verano/Invierno'},
        {name:'Tipo de Tela/material', value:' tela compra'},
        {name:'Genero', value:'Masculino/Femenino'},
        {name:'Talles', value:['3','4','5']},
        {name:'Color/es', value:['Blanco','Celeste','Multicolor']},
        {name:'medida', value:'40x50 cm '},
      ],
      img:[
        "./.././../../../../assets/Prendas/bolsas/modelo2/bolsa1.jpeg",
        "./.././../../../../assets/Prendas/bolsas/modelo2/bolsa2.jpeg",
        "./.././../../../../assets/Prendas/bolsas/modelo2/bolsa3.jpeg",
        "./.././../../../../assets/Prendas/bolsas/modelo2/bolsa4.jpeg",

      ]
    },
    Descripcion:[
      {name:'BOLSA DISEBLE',
        value:'Es bueno elegir bolsas reutilizables para llevar en el coche, en el bolso o en la mochila. Para hacer la compra, es fundamental tener un carro y acordarse de lo que queremos comprar, llevando una lista.'},
      {name:'RECOMENDACION',
        value:`
        Si se quiere una bolsa práctica que no ocupa nada y que se pueda guardar en un bolsillo , en el bolso o dejar en el lateral del coche para cuando  haga falta, esta bolsa tipo crochet es perfecta. Por menos tienes una bolsa con gran capacidad y no ocupa prácticamente nada.
        `}
    ]
  },
  {
    id:5,
    tittle:'Campera',
    discount:5,
    data:'Campera de Egresado',
    price:850,
    img:'../../../assets/Prendas/campera/campera1.jpeg',
    caracteristicas:{
      detalles:[
        {name:'Marca', value:'Marca de Campera'},
        {name:'Temporada', value:'invierno'},
        {name:'Tipo de Tela/material', value:' tela de campera'},
        {name:'Genero', value:'Masculino/Femenino'},
        {name:'Talles', value:['1','2','3','4','5']},
        {name:'Color/es', value:['Blanco','Negro','Multicolor']},
        {name:'medida', value:'NxM'},
      ],
      img:[
        "./.././../../../../assets/Prendas/campera/campera1.jpeg",
        "./.././../../../../assets/Prendas/campera/campera2.jpeg",
        "./.././../../../../assets/Prendas/campera/campera3.jpeg",
        "./.././../../../../assets/Prendas/campera/campera4.jpeg",
      ]
    },
    Descripcion:[
      {name:'Buzos Y Camperas De Egresados',
        value:'Somos fabricantes y nos dedicamos a la confección de prendas para egresados.'},
      {name:'¿SON LOS UNICOS DISEÑOS QUE TIENEN?',
        value:`
        ¡NO! Tenemos muchos diseños disponibles. Podes hacer uso de los que tenemos o bien consultarnos por privado por diseños especiales a medida.
        `},
      {name:'¿CUANTO DEMORA?',value:'La demora para la producción de las prendas es de 45 días aproximadamente.'},
      {name:'¿EN QUE TELA ESTAN CONFECCIONADOS?',value:`
      Nuestras camperitas y buzos están confeccionados en Ketten, que es una tela super liviana, frizada y brillante. Se utiliza principalmente para prendas escolares y deportivas.
      Utilizamos esta tela porque es ideal para el tipo de estampados que aplicamos, para que queden bien nítidos y no pierdan color con el uso. A su vez, es perfecta porque es resistente a los lavados y el uso diario.
      `}
    ]
  },
  
  ];
   
