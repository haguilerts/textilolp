import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu-left',
  templateUrl: './menu-left.component.html',
  styleUrls: ['./menu-left.component.scss']
})
export class MenuLeftComponent implements OnInit {
  classActive:any[];
  constructor() {
    this.classActive=[null,null,null,null]
   }

  ngOnInit(): void {
    this.classActive[0]='active'
  }
  private activarItem(id:number){
    for (let item=0; item<this.classActive.length; item++) {
      if (item===id) {             
        this.classActive[id]='active'
        console.log(item,id)
      }
      this.classActive[item]=null
    }
  }
  activar(name:string){
    switch(name){
      case 'talla':
        this.activarItem(0)
        break;
      case 'prenda':
        this.activarItem(1)
        break;
      case 'temporada':
        this.activarItem(2)
        break;
      case 'oferta':
        this.activarItem(3)
        break;
      
    }
    
    const optionsTallas=document.querySelector('.link-Talla');
    
  }
}
