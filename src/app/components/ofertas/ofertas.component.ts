import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cards } from 'src/app/shader/interfaces/cards';
import { CardsService } from 'src/app/shader/services/cards.service';
//import { carrucel } from '';

@Component({
  selector: 'app-ofertas',
  templateUrl: './ofertas.component.html',
  styleUrls: ['./ofertas.component.scss']
})
export class OfertasComponent implements OnInit {

  arrayOodertas:Cards[]=[]
  constructor(private cardServis:CardsService, private router:Router) { }

  ngOnInit(): void {
    this.getCardsOfertas();
  }
  
  async getCardsOfertas(){
    (await this.cardServis.getAll()).map(i=> this.arrayOodertas.push(i))
  }
  selectPrenda(prenda:string){
    console.log(prenda)
    this.router.navigate(['Prenda']); 
  }
  adelante(){
    
  }
}
