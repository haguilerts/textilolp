import { Component, ElementRef, OnInit, ViewChild, Renderer2, Input } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  @ViewChild('Sliderr') Sliderr: ElementRef;
  @ViewChild('imgPrincipal') imgfondo: ElementRef;
  @Input('myDetalles') imgs:any
  objs:any
  constructor(private render:Renderer2) { 
    this.Sliderr=new ElementRef(ElementRef)
    this.imgfondo=new ElementRef(ElementRef)
    this.objs=this.Sliderr.nativeElement.children
    }

  ngOnInit(): void {
    for (const obj in this.objs) {
      this.objs[0].classList?.add('active')
      this.objs[obj].classList?.add('')
    }
    
  }
  ngAfterViewInit(){
     //console.log(this.slider.nativeElement.scrollLeft);
     //console.log(this.imgfondo.nativeElement.src);
   /*  classArr.forEach(element =>{
      this.render.listen(element, 'click', (target)=>{
        console.log('clicked', target);
      })
    }); */
  //  console.log("imgs: ",this.imgs)
  }
  btn_left(){
    this.Sliderr.nativeElement.scrollLeft-=100   
  }
  btn_ringh(){
   // console.log('ring')
    this.Sliderr.nativeElement.scrollLeft+=100
  }
  mouseover(e:any){
    console.log('mouseover')
    this.imgfondo.nativeElement.src=e.target.src;
    let objs=this.Sliderr.nativeElement.children
    for (const obj in objs) {
      objs[obj].className='.';
      console.log(objs[obj].classList)
     };
  }
  activar(e:any){
    e.target.className='active'    
    //console.log('event: ',e.target.className)
  }
 
}
