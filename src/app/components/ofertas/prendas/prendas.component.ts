//import { query } from '@angular/animations';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as Glider from 'glider-js';
import { Cards } from 'src/app/shader/interfaces/cards';
import { CardsService } from 'src/app/shader/services/cards.service';
//import Glide from './@glidejs/glide'
@Component({
  selector: 'app-prendas',
  templateUrl: './prendas.component.html',
  styleUrls: ['./prendas.component.scss']
})
export class PrendasComponent implements OnInit {
  @ViewChild('glider') glider: any=jQuery('.glider');
    card:Cards;
    color:string[]=[]
    talle:string[]=[]
  
  constructor( private paramURL:ActivatedRoute,private prendServis:CardsService ) { 
    this.card={
      id:1,
      tittle:'',
      discount:10,
      data:'',
      price:5,
      img:'../../../assets/Prendas/sombresos/sombrero1.jpeg',
      caracteristicas:{
        detalles:[        
          {name:'Marca', value:'marca de Campera'},
          {name:'Temporada', value:'invierno'},
          {name:'Tipo de Tela/material', value:' tela de campera'},
          {name:'Genero', value:'Masculino/Femenino'},
          {name:'Talles', value:['1','2','3','4','xxxxx']},
          {name:'Color/es', value:['Blanco', 'Negro', 'xxxxx']},
          {name:'medida', value:''},
        ],
        img:[
          "./../../../../../assets/Prendas/sombreros/sombrero1.jpeg",
          "./../../../../../assets/Prendas/sombreros/sombrero2.jpeg",
          "./../../../../../assets/Prendas/sombreros/sombrero3.jpeg",
        ]
      }
    };

  

    //console.log("talle: ", this.card.caracteristicas?.detalles[4].value)
    //console.log("color: ", this.card.caracteristicas?.detalles[5].value)

  }

  ngOnInit(): void {
    
  }
  async ngAfterViewInit(){
   //this.talle.push() 
   const params = this.paramURL.snapshot.params;
    console.log('params: ',params.id)
   if (params) {
     try {
      this.card =(await this.prendServis.getById(params.id))
      this.talle=this.card.caracteristicas?.detalles[4].value
      this.color=this.card.caracteristicas?.detalles[5].value
      console.log('card: ', this.card)

     } catch (error) {
       console.error(error)
     }
   }
   //this.glider=jQuery('.glider')
   new Glider( this.glider, {
    slidesToShow: 1,
    slidesToScroll: 1,
    draggable: true,
    dots: '.dots',
    arrows: {
        prev: '.glider-prev',
        next: '.glider-next'
    },
    responsive: [{
        // screens greater than >= 775px
        breakpoint: 555,
        settings: {
            // Set to `auto` and provide item width to adjust to viewport
            slidesToShow: 3,
            slidesToScroll: 1,
            itemWidth: 150,
            duration: 0.25
        }
    }, {
        // screens greater than >= 775px
        breakpoint: 775,
        settings: {
            // Set to `auto` and provide item width to adjust to viewport
            slidesToShow: 4,
            slidesToScroll: 1,
            itemWidth: 150,
            duration: 0.25
        }
    }, {
        // screens greater than >= 1024px
        breakpoint: 1024,
        settings: {
            slidesToShow: 5,
            slidesToScroll: 1,
            itemWidth: 150,
            duration: 0.25
        }
    }]
});
  }



  /* ngOnChanges(changes:SimpleChange){   
   
  } */

}
