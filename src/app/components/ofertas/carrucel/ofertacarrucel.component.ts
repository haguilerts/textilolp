
import * as jQuery from 'jquery';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ofertacarrucel',
  templateUrl: './ofertacarrucel.component.html',
  styleUrls: ['./ofertacarrucel.component.scss']
})
export class OfertacarrucelComponent implements OnInit {
  
  constructor() { 
   
    
  }

  ngOnInit(): void {
    
    setInterval(()=>{
      this.irAdelante();
    },9000)   
    
  }
 

  irAdelante(){
      let sliderSectionFirst = document.querySelectorAll(".slider__section")[0];  
      jQuery('#slider').append(sliderSectionFirst)  
      jQuery('#slider').css('margin-left', '-13%');
     
  }
  irAtras(){
    let sliderSection = document.querySelectorAll(".slider__section");
    let sliderSectionLast = sliderSection[sliderSection.length - 1]
    jQuery('#slider').prepend(sliderSectionLast)     
    jQuery('#slider').css('margin-left', '-13%');
   
  }
}
