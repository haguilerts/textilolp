import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import { Router } from '@angular/router';
import { Cards } from 'src/app/shader/interfaces/cards';
import { CardsService } from 'src/app/shader/services/cards.service';
import { OrderBy } from './orderBy';
@Component({
  selector: 'app-prenda',
  templateUrl: './prenda.component.html',
  styleUrls: ['./prenda.component.scss']
})
export class PrendaComponent implements OnInit {
  @Input() typeCard:string
  prendas:Cards[];
  private allPrendas:Cards[]=[]
  cardsTemporary:Cards[]=[]

  ordering:OrderBy;
  constructor(private servis:CardsService, private ruta:Router ) {
    this.prendas=[{
      id:1,
      tittle:'Sombrero',
      discount:10,
      data:'Tapa Dura',
      price:1000,
      img:'../../../assets/Prendas/sombreros/sombrero1.jpeg',
      caracteristicas:{
        detalles:[
          {name:'Marca', value:'marca de Campera'},
          {name:'Temporada', value:'invierno'},
          {name:'Tipo de Tela/material', value:' tela de campera'},
          {name:'Genero', value:'Masculino/Femenino'},
          {name:'Talles', value:'1,2,3,4,5'},
          {name:'Color/es', value:['Blanco', 'Negro', 'Multicolor']},
          {name:'medida', value:''},
        ],
        img:[
          "./.././../../../../assets/Prendas/sombreros/sombrero1.jpeg",
          "./.././../../../../assets/Prendas/sombreros/sombrero2.jpeg",
          "./.././../../../../assets/Prendas/sombreros/sombrero3.jpeg",
        ]
      }
    }]
    this.ordering=new OrderBy(this.prendas)
    this.typeCard='todos'
  }

  async ngOnInit(){
    try {
      this.prendas= await this.servis.getAll();
     // this.ordering=new OrderBy(this.allCards)
      //this.prendas= this.allCards
      this.cardsTemporary=this.allPrendas
      //console.log(this.allCards)
    } catch (error) {
      console.error(error)
    }
  } 
  ngOnChanges(changes:SimpleChange){   
    console.log('input: ',this.typeCard)
    this.fitrarPor(this.typeCard)   
  }

  private fitrarPor(option:string){
    this.ordering=new OrderBy(this.cardsTemporary)
    switch(option){
      case 'Price-men_may':
        this.allPrendas=this.ordering.desPrice()
        break;
      case 'Price-may_men':
        this.allPrendas=this.ordering.ascPrice()
        break;
      case 'Year-men_may':
        //this.cards=this.ordering.desYear()
        break;
      case 'Year-may_men':
        //this.cards=this.ordering.ascYear()
        break;
      case '':
        break;
      default:
        this.definirAutos(option)
        break;
    }  
  } 
  private definirAutos(tipo:string){
    var auto:Cards[]=[];
    console.log(this.prendas)
    this.prendas.forEach(e => {
      try {     
        console.log('e: ',e)  
       /*  if(e.caracteristicas?.modelo==tipo){   
          console.log(e)      
          auto.push(e)
        }else if(tipo=='todos'){          
          //auto=this.allPrendas
        } */
      } catch (error) {
        console.error(error)
      }        
    });
    this.cardsTemporary=auto
    this.prendas=auto 
    console
  }

}
