import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopRegisterComponent } from './top-register.component';

describe('TopRegisterComponent', () => {
  let component: TopRegisterComponent;
  let fixture: ComponentFixture<TopRegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopRegisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
