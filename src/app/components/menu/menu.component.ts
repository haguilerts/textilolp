import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery'
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  mostrarMenu:string[];
  on:boolean
  activo:any[];
  constructor( private ruta:Router) { 
    this.mostrarMenu=[]
    this.on=true
    this.activo=[null,null,null,null]
  }

  ngOnInit(): void {
    this.mostrarMenu=['']
    this.activo[0]='active'
  }
  onclickResposil(id:any){
    //menu_vuelta / 
    const menu=document.querySelector('.direc-menu')
    console.log(menu?.classList.contains('direc-menu_ida'))
    this.on=!this.on
    if(menu?.classList.item(2)==='direc-menu_ida'){
      this.mostrarMenu[0]='direc-menu_vuelta'
    }else{
      this.mostrarMenu[0]='direc-menu_ida'
    }

    console.log(this.mostrarMenu[0])
  }
  cambiarPage(page:string){
    console.log(page)
    this.ruta.navigate([page]);
    this.cambiarLinck(page)
    //filtro de tamanio responsil  
    /* if (jQuery(window).scrollTop() < 646) {

    } */
  }
  private clearLinck(){
    for (const i in this.activo) {
      this.activo[i]=null
    }
  }
  private cambiarLinck(linkPage:string){
    switch(linkPage){
      case 'home':
        this.clearLinck();
        this.activo[0]='active'
        break;
      case 'nosotros':
        this.clearLinck();
        this.activo[1]='active'
        break;
      case 'contacto':
        this.clearLinck();
        this.activo[2]='active'
        break;
      case 'pregunta':
        this.clearLinck();
        this.activo[3]='active'
        break;
    }
  }
  cartas(simbolo:any[]){
    let misCartas=[]
    for (let i = 0; i < simbolo.length; i++) {
      if(simbolo[i]!=8 || simbolo[i]!=9 ){
        misCartas.push(simbolo[i])
      }      
    }
    return misCartas;
  }
  scrollVertical(){
    
  }
}
