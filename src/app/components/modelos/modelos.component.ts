import { Component, OnInit } from '@angular/core';
import { Producto } from '../../shader/BBDD/cardsBD';
import { CardsService } from '../../shader/services/cards.service';

@Component({
  selector: 'app-modelos',
  templateUrl: './modelos.component.html',
  styleUrls: ['./modelos.component.scss']
})
export class ModelosComponent implements OnInit {

  myOrder:string=''
  public isLoading = false;
  public src: string;
  public data$: any;//
  constructor( private apiRest:CardsService) {
    this.src=''
   }

  ngOnInit(): void {
  }
  
  selectOption(filtro:string){
    this.myOrder=filtro
  }
  search(value: any): any {
    console.log('--> ',value)
    this.isLoading = true;
    this.data$ = this.apiRest.getAll()
      /* .pipe(
          map(project:({tracks}) => tracks.items),
        //finalize(() => this.isLoading = false)
      ) */
    console.log(this.data$)
  }

}
