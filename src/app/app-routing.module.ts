import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrendasComponent } from './components/ofertas/prendas/prendas.component';
import { ContactoComponent } from './pages/contacto/contacto.component';
import { HomeComponent } from './pages/home/home.component';
import { SobreNosotrosComponent } from './pages/sobre-nosotros/sobre-nosotros.component';

const routes: Routes = [
  {path:'', pathMatch:'full',redirectTo:'/'},
  {path:'', component: HomeComponent},
  {path:'nosotros', component: SobreNosotrosComponent},
  {path:'contacto', component: ContactoComponent},


  {path:'prenda/:id', component: PrendasComponent},

  {path:'**', redirectTo:'/'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
        scrollPositionRestoration: 'enabled',
        initialNavigation: 'enabled',
    }),
  ],
  /* imports: [RouterModule.forRoot(routes)], */
  exports: [RouterModule]
})
export class AppRoutingModule { }
